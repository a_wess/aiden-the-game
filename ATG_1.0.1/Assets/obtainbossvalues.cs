﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class obtainbossvalues : MonoBehaviour
{
    //the values of the boss
    public static float bosshealth;
    // Use this for initialization
    void Start()
    {
        bosshealth = 300;
    }

    // Update is called once per frame
    void Update()
    {
        if (bosshealth <= 0)
        {
            SceneManager.LoadScene("Win");
            //the player wins the game if the boss' health reaches 0
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HP_HUD : MonoBehaviour {
    //the HUD controls for text
    public Text TextH3;
    public Text txtSalt;
    public Text txtMemes;
    public Text txtPizzaCokes;
    public Text txtCrackerJacks;
    public Text txtRAWSteaks;
    public Text txtMKs;
    public Text txtScore;
    public Text txtMoney;
    public Text txtLevelScore;
 
	void Start () {
		
	}
	

	void Update () {
        TextH3.text = obtainplayervalues.CurrentAidy_HP.ToString() + "/200";
        txtSalt.text = obtainplayervalues.OwnedSalt.ToString();
        txtMemes.text = obtainplayervalues.OwnedMemes.ToString();
        txtPizzaCokes.text = obtainplayervalues.OwnedPizzaCoke.ToString();
        txtCrackerJacks.text = obtainplayervalues.OwnedCrackerJacks.ToString();
        txtRAWSteaks.text = obtainplayervalues.OwnedRAWSteak.ToString();
        txtMKs.text = obtainplayervalues.OwnedMK.ToString();
        txtScore.text = "You killed " + obtainplayervalues.kills.ToString() + " enemies!";
        txtMoney.text = "£" + obtainplayervalues.CurrentMoney.ToString();
        txtLevelScore.text = (-1 * obtainplayervalues.LevelEnemyCount).ToString();
        
        //this displays the health,money,inventory stats, remaining enemies and score through text when necassary


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile_attack : MonoBehaviour {
    //the projectile enemies ability to attack
    public bool canthrow;
    private List<GameObject> Projectilesenemyleft = new List<GameObject>();
    private List<GameObject> Projectilesenemyright = new List<GameObject>();
    private List<GameObject> Projectilesenemyup = new List<GameObject>();
    private List<GameObject> Projectilesenemydown = new List<GameObject>();
    public GameObject sword_projectile;
    public float sanics;
    public float wait;
    private float projectilecounter;
    public GameObject fireSwordenemyleft;
    public GameObject fireSwordenemyright;
    public GameObject fireSwordenemydown;
    public GameObject fireSwordenemyup;
    public GameObject projectingenemy;
    bool enemyalive;

    // Use this for initialization
    void Start () {
        enemyalive = true;
        canthrow = false;
        projectilecounter = wait;

    }

    // Update is called once per frame
    private void OnBecameVisible()
    {
        canthrow = true;
    }

    void Update()
    {
        projectilecounter -= Time.deltaTime;
        //this acts as a timer between firing of projectiles
        if (canthrow == true)
        {
           
            if (projectilecounter < 0)
            {
                //these create a projectile for each direction and stop it from colliding with the player
                projectilecounter = wait;
                GameObject throwing_swordenemyleft = (GameObject)Instantiate(sword_projectile, transform.position, Quaternion.identity);
                Projectilesenemyleft.Add(throwing_swordenemyleft);
                Physics2D.IgnoreCollision(projectingenemy.GetComponent<BoxCollider2D>(), throwing_swordenemyleft.GetComponent<BoxCollider2D>());
                GameObject throwing_swordenemyright = (GameObject)Instantiate(sword_projectile, transform.position, Quaternion.identity);
                Projectilesenemyright.Add(throwing_swordenemyright);
                Physics2D.IgnoreCollision(projectingenemy.GetComponent<BoxCollider2D>(), throwing_swordenemyright.GetComponent<BoxCollider2D>());
                GameObject throwing_swordenemyup = (GameObject)Instantiate(sword_projectile, transform.position, Quaternion.identity);
                Projectilesenemydown.Add(throwing_swordenemyup);
                Physics2D.IgnoreCollision(projectingenemy.GetComponent<BoxCollider2D>(), throwing_swordenemyup.GetComponent<BoxCollider2D>());
                GameObject throwing_swordenemydown = (GameObject)Instantiate(sword_projectile, transform.position, Quaternion.identity);
                Projectilesenemyright.Add(throwing_swordenemydown);
                Physics2D.IgnoreCollision(projectingenemy.GetComponent<BoxCollider2D>(), throwing_swordenemydown.GetComponent<BoxCollider2D>());
                Physics2D.IgnoreCollision(throwing_swordenemyup.GetComponent<BoxCollider2D>(), throwing_swordenemyright.GetComponent<BoxCollider2D>());
            }
        }
        // each of these for statements moves the projectile in a specific direction
                for (int i = 0; i < Projectilesenemyleft.Count; i++)
                {
                    fireSwordenemyleft = Projectilesenemyleft[i];
                    if (fireSwordenemyleft != null)
                    {
                        fireSwordenemyleft.transform.Translate(new Vector3(-1, 0) * Time.deltaTime * sanics);
                        
                      
                    }
                }

        for (int i = 0; i < Projectilesenemyup.Count; i++)
        {
            fireSwordenemyup = Projectilesenemyup[i];
            if (fireSwordenemyup != null)
            {

                fireSwordenemyup.transform.Translate(new Vector3(0, 1) * Time.deltaTime * sanics);

             

            }
        }

        for (int i = 0; i < Projectilesenemyright.Count; i++)
                {
                    fireSwordenemyright = Projectilesenemyright[i];
                    if (fireSwordenemyright != null)
                    {

                        fireSwordenemyright.transform.Translate(new Vector3(1, 0) * Time.deltaTime * sanics);
                        
                      

                    }


                }
              


                for (int l = 0; l < Projectilesenemydown.Count; l++)
                {
                    fireSwordenemydown = Projectilesenemydown[l];
                    if (fireSwordenemydown != null)
                    {

                        fireSwordenemydown.transform.Translate(new Vector3(0, -1) * Time.deltaTime * sanics);
                    
                    

                    }

                }

                if(enemyalive == false)
        {
          
        }
            }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "AidyProjectile(Clone)")
        {
            enemyalive = false;
        }
        //this kills the enemy when it is hit by a shuriken and turns the projectile into a mine
    }
}
    
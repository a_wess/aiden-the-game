﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ignore_attack : MonoBehaviour {
    public GameObject player;
	// Use this for initialization
	void Start () {
        Physics2D.IgnoreCollision(player.GetComponent<Collider2D>(), GetComponent<Collider2D>());
    }
	
	// Update is called once per frame
	void Update () {
        Physics2D.IgnoreCollision(player.GetComponent<Collider2D>(), GetComponent<Collider2D>());

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
      

        if (collision.gameObject.name == "Player")
            Debug.Log("collided with " + collision.gameObject.name);
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collision.collider.GetComponent<Collider2D>());
    }
}

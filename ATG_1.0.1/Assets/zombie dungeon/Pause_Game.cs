﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause_Game : MonoBehaviour {
    //the controls of the games menus
    public Transform canvas;
    public Transform Eat;
    public Transform Saves;
    public Transform Loads;
    public Camera MainCamera;
    void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
        {
           Eat.gameObject.SetActive(false);

            STOP();
            //pauses the game when the player presses the Escape key
        }
         
}
    public void STOP()
    {
        if (canvas.gameObject.activeInHierarchy == false)
        {
            canvas.gameObject.SetActive(true);
            Time.timeScale = 0;
            PlayerMovement.canmove = false;
            AidyAttack.canthrow = false;
            Cursor.visible = true;
            //this stops the game, makes the cursor visible and loads the game menu 
        }
        else
        {
            canvas.gameObject.SetActive(false);
            if (Eat.gameObject.activeInHierarchy == false)
            {
                Time.timeScale = 1;
                Cursor.visible = false;
                PlayerMovement.canmove = true;
                AidyAttack.canthrow = true;
            }
            //this resumes the game
           
        }

      
    }
    public void ExitGame()
    {
        {
            Application.Quit();

        //this exits the game
        }
    }

  

    public void LoadEat()
    {
        STOP();
        if (Eat.gameObject.activeInHierarchy == false)
        {
            Time.timeScale = 0;
            PlayerMovement.canmove = false;
            AidyAttack.canthrow = false;
            Eat.gameObject.SetActive(true);
            Cursor.visible = true;
            //This loads the eat menu       
        }
        else
        {
            if (canvas.gameObject.activeInHierarchy == false)
            {
                Time.timeScale = 1;
                PlayerMovement.canmove = true;
                AidyAttack.canthrow = true;
                Eat.gameObject.SetActive(false);
                Cursor.visible = false;
//closes the menu, hides the cursor and unpauses the game
            }
        }
    }

    public void LoadSave()
    {
        STOP();
        if (Saves.gameObject.activeInHierarchy == false)
        {
            Time.timeScale = 0;
            PlayerMovement.canmove = false;
            AidyAttack.canthrow = false;
           Saves.gameObject.SetActive(true);
            Cursor.visible = true;
            //This loads the save menu       
        }
        else
        {
            if (canvas.gameObject.activeInHierarchy == false)
            {
                Time.timeScale = 1;
                PlayerMovement.canmove = true;
                AidyAttack.canthrow = true;
                Saves.gameObject.SetActive(false);
                Cursor.visible = false;
                //closes the menu, hides the cursor and unpauses the game
            }
        }
    }

    public void LoadLoad()
    {
        //best name I could come up with
        STOP();
        if (Loads.gameObject.activeInHierarchy == false)
        {
            Time.timeScale = 0;
            PlayerMovement.canmove = false;
            AidyAttack.canthrow = false;
            Loads.gameObject.SetActive(true);
            Cursor.visible = true;
            //This loads the Load menu       
        }
        else
        {
            if (canvas.gameObject.activeInHierarchy == false)
            {
                Time.timeScale = 1;
                PlayerMovement.canmove = true;
                AidyAttack.canthrow = true;
                Loads.gameObject.SetActive(false);
                Cursor.visible = false;
                //closes the menu, hides the cursor and unpauses the game
            }
        }
    }

    public void ExitEat()
    {
        if (Eat.gameObject.activeInHierarchy == true)
        {
            STOP();
            Eat.gameObject.SetActive(false);

            //closes the eat menu
        }
    }

    public void ExitSave()
    {
        if (Saves.gameObject.activeInHierarchy == true)
        {
            STOP();
            Saves.gameObject.SetActive(false);

            //closes the save menu
        }
    }

    public void ExitLoad()
    {
        if (Loads.gameObject.activeInHierarchy == true)
        {
            Loads.gameObject.SetActive(false);

            //closes the load menu
        }
    }
}

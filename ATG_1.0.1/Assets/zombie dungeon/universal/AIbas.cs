﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIbas : MonoBehaviour {
    //the enemy melee movement
    public float enemy_health = 20;
    public float slimez_speed;
    public Transform player;
    public bool canmove;
    public GameObject spinningcoin;
    public float damage;
    private List<GameObject> Coins = new List<GameObject>();
    public static bool SetDamage;
   
    void Start () {
        enemy_health = 20;
   
}

   
    private void OnBecameVisible()
    {
        canmove = true;
        //the enemy can move when it comes into the camera's range
    }

  
    void Update () {


        if (canmove)
        {
            Vector2 toTarget = GameObject.Find("Player").transform.position - transform.position;
            transform.Translate(toTarget * slimez_speed * Time.deltaTime);
            //when the enemy is visible it is activated and can move towards the player
        }

        if (enemy_health <= 0)
        {
            obtainplayervalues.LevelEnemyCount = obtainplayervalues.LevelEnemyCount - 1;
            obtainplayervalues.ThisLevelScore = obtainplayervalues.ThisLevelScore + 1;
            GameObject newCoin = (GameObject)Instantiate(spinningcoin, transform.position, Quaternion.identity);
            Coins.Add(newCoin);
            obtainplayervalues.kills = obtainplayervalues.kills + 1;
            Debug.Log(obtainplayervalues.kills + "kills");
            gameObject.SetActive(false);
            
            //enemy set inactive and drops a coin
        }
      if (obtainplayervalues.SharpShuriken)
        {
            damage = 20;
        }
      else
        {
            damage = 10;
        }

     
	}

    void OnCollisionEnter2D(Collision2D collision)
    {


        string collider = collision.gameObject.name;
        if (collider == "AidyProjectile(Clone)")
        {
            enemy_health = enemy_health - damage;
        }
        if (collider == "sword_right(Clone)" || collider == "sword_left 1(Clone)" ||
            collider == "sword_backward(Clone)" || collider == "sword_forward(Clone)")
        {
            enemy_health = enemy_health - damage;
        }

      //this kills the enemy when it is hit by a shurkien
    }

    public void BuyShuriken()
    {

        if (obtainplayervalues.CurrentMoney > 150 && obtainplayervalues.SharpShuriken)
        {
            obtainplayervalues.SharpShuriken = true;
            obtainplayervalues.CurrentMoney = obtainplayervalues.CurrentMoney - 150;
          

        }
        //this removes the price of a from the players current money and adds one when the player clicks the button
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float AidySpeed;
    public static bool canmove;
    // Update is called once per frame

    private void Start()
    {
        canmove = true;
    }
    void Update()
    {
        if (canmove)
        {
            if (Input.GetKey(KeyCode.W))
            {
                obtainplayervalues.Facing = obtainplayervalues.Direction.FORWARD;
                transform.Translate(Vector2.up * AidySpeed);
            }
            if (Input.GetKey(KeyCode.S))
            {
                obtainplayervalues.Facing = obtainplayervalues.Direction.BACKWARD;
                transform.Translate(-Vector2.up * AidySpeed);
            }
            if (Input.GetKey(KeyCode.D))
            {
                obtainplayervalues.Facing = obtainplayervalues.Direction.RIGHT;

                transform.Translate(Vector2.right * AidySpeed);
            }
            if (Input.GetKey(KeyCode.A))
            {
                obtainplayervalues.Facing = obtainplayervalues.Direction.LEFT;
                transform.Translate(-Vector2.right * AidySpeed);
            }
        }
        //these are the controls to move the playable character around the screen. The popular 'WASD' setup is used here
    }
}
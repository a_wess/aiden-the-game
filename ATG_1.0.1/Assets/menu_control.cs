﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class menu_control : MonoBehaviour
{

    //the controls for the buttons on the main menu
    public void NewGame() {
       
        SceneManager.LoadScene("walker");
        Cursor.visible = false;
        obtainplayervalues.CurrentMoney = 0;

        for (int i = 0; i < obtainplayervalues.levels.Length; i++)
        {
            obtainplayervalues.levels[i] = false;
        }

        obtainplayervalues.victory = false;
        obtainplayervalues.alive = true;
        obtainplayervalues.OwnedCrackerJacks = 0;
        obtainplayervalues.OwnedMemes = 0;
        obtainplayervalues.OwnedRAWSteak = 0;
        obtainplayervalues.OwnedSalt = 0;
        obtainplayervalues.OwnedPizzaCoke = 0;
        obtainplayervalues.OwnedMK = 0;
        //this starts a the game at the shop with an empty inventory, no money and no progress

    }
    public void GetOut()
    {
        Application.Quit();

        //this exists the game
    }


    public void MainMenu()
    {
        SceneManager.LoadScene("Main Menu");
        //loads the menu
    }

}







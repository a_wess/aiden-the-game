﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class obtainplayervalues : MonoBehaviour {
    //the values for the player's character

    public enum Direction
    {
        LEFT,
        RIGHT,
        FORWARD,
        BACKWARD
    }

    public static float CurrentMoney;
    public static int StartAidy_HP = 200;
    public static float CurrentAidy_HP;
    public static bool[] levels = new bool[4];
    public static bool victory;
    public static bool alive;
    public static int OwnedSalt = Mathf.Clamp(OwnedSalt, 0, 100);
    public static int OwnedMemes = Mathf.Clamp(OwnedMemes, 0, 100);
    public static int OwnedPizzaCoke = Mathf.Clamp(OwnedPizzaCoke, 0, 100);
    public static int OwnedCrackerJacks = Mathf.Clamp(OwnedCrackerJacks, 0, 100);
    public static int OwnedRAWSteak = Mathf.Clamp(OwnedRAWSteak, 0, 100);
    public static int OwnedMK = Mathf.Clamp(OwnedMK, 0, 100);
    public GameObject enemy;
    public GameObject projectiler;
    public float magnitude = 3;
    public GameObject Player;
    public GameObject Door;
    public GameObject coin;
    public AudioClip PlayerDamagedSound;
    public static bool swordowned;
    public static int kills;
    public static bool FastShuriken;
    public static bool SharpShuriken;
    public static bool invincible;
    public static int ThisLevelScore;
    public static int LevelEnemyCount;
    public static Direction Facing;
    public float timeSinceHit;
    


    void Start() {
        ThisLevelScore = 0;
        CurrentAidy_HP = 200;
        Facing = Direction.FORWARD;
        // CurrentMoney = 0;
        


        //this sets the HP, the money to 0 and win as 200 and false
    }


    void Update() {

        if (timeSinceHit < 0)
        {
            invincible = false;
            this.GetComponent<SpriteRenderer>().color = Color.white;
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

        }
        else
            timeSinceHit -= Time.deltaTime;
        if (CurrentMoney < 0) {

            CurrentMoney = CurrentMoney * 0;
        }
        //this stops the money from being negative


        Physics2D.IgnoreCollision(Player.GetComponent<BoxCollider2D>(), Door.GetComponent<BoxCollider2D>());


        if (CurrentAidy_HP <= 0) {
            alive = false;
            CurrentAidy_HP = 0;
            SceneManager.LoadScene("death_screen");
            //when the players health reaches 0 or lower the players unsaved progress is lost and the death screen is loaded
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (!invincible)
        {
            if (collision.gameObject.name == "slime")
            {
                AudioSource.PlayClipAtPoint(PlayerDamagedSound, transform.position);
                CurrentAidy_HP = CurrentAidy_HP - 15;


                //the player takes 15 damage when they collide with a slime enemy
            }

            if (collision.gameObject.name == "anarchist" || collision.gameObject.name == "zombie" || collision.gameObject.name == "robots" || collision.gameObject.name == "SlimeProjectile(Clone)")
            {
                AudioSource.PlayClipAtPoint(PlayerDamagedSound, transform.position);
                CurrentAidy_HP = CurrentAidy_HP - 10;

                //all enemies except for the final boss deal 10 damage to the player when they collide with the player
            }

            if (collision.gameObject.name == "boss")
            {
                CurrentAidy_HP = CurrentAidy_HP - 20;
            }
        }
        //the player takes 20 damage when they hit the boss
        if (collision.gameObject.name == "anarchist" || collision.gameObject.name == "zombie" || collision.gameObject.name == "robots" || collision.gameObject.name == "SlimeProjectile(Clone)" 
            || collision.gameObject.name == "slime" || collision.gameObject.name == "boss")
        {
            GoInvincible(collision.gameObject);
        }
    }

    public static bool BossUnlocked()
    {
        foreach (bool level in levels)
        {
            if (!level)
            {
                return false;
            }
        }
        return true;
    }

    public void GoInvincible(GameObject attacker)
    {
        timeSinceHit = 0.5F;
        this.GetComponent<SpriteRenderer>().color = Color.grey;
        this.GetComponent<Rigidbody2D>().velocity = new Vector2((transform.position.x - attacker.transform.position.x) * 10, (transform.position.y - attacker.transform.position.y) * 10);
        invincible = true;

    }
}


   




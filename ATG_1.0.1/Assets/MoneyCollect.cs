﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyCollect : MonoBehaviour
{
    public GameObject enemy;
    public AudioClip ding;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Physics2D.IgnoreCollision(gameObject.GetComponent<BoxCollider2D>(), enemy.GetComponent<BoxCollider2D>());
        //the enemies do not collide with coins
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            AudioSource.PlayClipAtPoint(ding, transform.position);
            obtainplayervalues.CurrentMoney = obtainplayervalues.CurrentMoney + 5;
            DestroyObject(gameObject);
            // when the player collides with a coin; the coin collect sound is played, the player earns 5 pounds and the coin is removed from the heirarchy
        }
        else
            Physics2D.IgnoreCollision(collision.collider.GetComponent<Collider2D>(), GetComponent<Collider2D>());
    } 
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileHit : MonoBehaviour {
    //the enemy projectile's values upon collision
    public GameObject enemy;
    public GameObject player;

	void Start () {
		
	}
	
	
	void Update () {
        Physics2D.IgnoreCollision(gameObject.GetComponent<BoxCollider2D>(), enemy.GetComponent<BoxCollider2D>());
     
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "WALL")
        {
            DestroyObject(gameObject);
            //this destroys the projectile when it hits a wall
        }

        if (collision.gameObject.name == "Player")
        {
         
            DestroyObject(gameObject);
            //the projectile is destroyed when it hits the player
        }
    }
}

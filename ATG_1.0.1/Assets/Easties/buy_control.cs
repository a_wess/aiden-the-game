﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class buy_control : MonoBehaviour {
    //each of these subroutines allows the player to buy an item from the store in the Walker level
    //the items can only be purchased if he playe has enough mumpoints
    //the specific item count is increased by one in the inventory

    public Text txtDescription;
    public void BuyOneSalt()
    {    
        if (obtainplayervalues.CurrentMoney >= 50)
        {
            obtainplayervalues.CurrentMoney = obtainplayervalues.CurrentMoney - 50;
            obtainplayervalues.OwnedSalt = obtainplayervalues.OwnedSalt + 1;
            Debug.Log(obtainplayervalues.OwnedSalt.ToString() + "salts");
            if (obtainplayervalues.OwnedSalt == 1)
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedSalt.ToString() + " grain of salt!";
            }
            else
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedSalt.ToString() + " grains of salt!";
            }
        }
        else
        {
            txtDescription.text = "you're too poor :(";
        }
        //this removes the price of a salt from the players current money and adds one salt when the player clicks the button
    }
  public void BuyOneMeme()
    {       
        if (obtainplayervalues.CurrentMoney >= 25)
        {
            obtainplayervalues.CurrentMoney = obtainplayervalues.CurrentMoney - 25;
            obtainplayervalues.OwnedMemes = obtainplayervalues.OwnedMemes + 1;
            Debug.Log(obtainplayervalues.OwnedMemes.ToString() + "memes");
            if (obtainplayervalues.OwnedMemes == 1)
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedMemes.ToString() + " dank meme!";
            }
            else
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedMemes.ToString() + " zesty memes!";
            }
        }
        else
        {
            txtDescription.text = "you're too poor :(";
        }
        //this removes the price of a meme from the players current money and adds one meme when the player clicks the button
    }
   public void BuyOnePizzaCoke()
    {  
        if (obtainplayervalues.CurrentMoney >= 40)
        {
            obtainplayervalues.CurrentMoney = obtainplayervalues.CurrentMoney - 40;
            obtainplayervalues.OwnedPizzaCoke = obtainplayervalues.OwnedPizzaCoke + 1;
            Debug.Log(obtainplayervalues.OwnedPizzaCoke.ToString() + "PC");
            if (obtainplayervalues.OwnedPizzaCoke == 1)
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedPizzaCoke.ToString() + " pizzacoke!";
            }
            else
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedPizzaCoke.ToString() + " pizzacokes!";
            }
        }
        else
        {
            txtDescription.text = "you're too poor :(";
        }
        //this removes the price of a pizzacoke from the players current money and adds one pizzacoke when the player clicks the button
    }
    public void BuyOneCrackerjacks()
    {      
        if (obtainplayervalues.CurrentMoney >= 80)
        {
            obtainplayervalues.CurrentMoney = obtainplayervalues.CurrentMoney - 80;
            obtainplayervalues.OwnedCrackerJacks = obtainplayervalues.OwnedCrackerJacks + 1;
            Debug.Log(obtainplayervalues.OwnedSalt.ToString() + "salts");
            if (obtainplayervalues.OwnedCrackerJacks == 1)
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedCrackerJacks.ToString() + " crackerjack!";
            }
            else
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedCrackerJacks.ToString() + " crackerjacks!";
            }
        }
        else
        {
            txtDescription.text = "you're too poor :(";
        }
        //this removes the price of a crackerjack from the players current money and adds one crackerjack when the player clicks the button
    }
     public void BuyOneRawSteak()
    {    
        if (obtainplayervalues.CurrentMoney >= 135)
        {
            obtainplayervalues.CurrentMoney = obtainplayervalues.CurrentMoney - 135;
            obtainplayervalues.OwnedRAWSteak = obtainplayervalues.OwnedRAWSteak + 1;
            Debug.Log(obtainplayervalues.OwnedRAWSteak.ToString() + "RAWSteak");
            if (obtainplayervalues.OwnedRAWSteak == 1)
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedRAWSteak.ToString() + " RAW Steak!";
            }
            else
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedRAWSteak.ToString() + " RAW Steaks!";
            }
        }
        else
        {
            txtDescription.text = "you're too poor :(";
        }
        //this removes the price of a RAW steak from the players current money and adds one RAW steak when the player clicks the button
    }
   public void BuyOneMK()
    {       
        if (obtainplayervalues.CurrentMoney > 300)
        {
            obtainplayervalues.CurrentMoney = obtainplayervalues.CurrentMoney - 300;
            obtainplayervalues.OwnedMK = obtainplayervalues.OwnedMK + 1;
            Debug.Log(obtainplayervalues.OwnedMK.ToString() + "MK");
            if (obtainplayervalues.OwnedMK == 1)
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedMK.ToString() + " MK!";
            }
            else
            {
                txtDescription.text = "You now own " + obtainplayervalues.OwnedMK.ToString() + " MKs!";
            }
        }
        else
        {
            txtDescription.text = "you're too poor :(";
        }
        //this removes the price of a from the players current money and adds one when the player clicks the button
    }


    
}

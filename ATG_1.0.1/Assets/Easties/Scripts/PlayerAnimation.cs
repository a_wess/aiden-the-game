﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {
    public Animator anime;
    public static bool Back;
    public static bool Front;
    public static bool Left;
    public static bool Right;
    public static bool BackWalk;
    public static bool FrontWalk;
    public static bool LeftWalk;
    public static bool RightWalk;

	//initialization
	void Start () {
        anime = GetComponent<Animator>();
        
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.W))
        {
            anime.SetBool("Back", true);

            anime.SetBool("Front", false);

            anime.SetBool("Right", false);

            anime.SetBool("Left", false);

            anime.SetBool("RightWalk", false);

            anime.SetBool("LeftWalk", false);

            anime.SetBool("FrontWalk", false);

            anime.SetBool("BackWalk", false);

          
            //This removes all the other possible animation directions and sets it to stand facing away from the viewer
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            anime.SetBool("Left", true);

            anime.SetBool("Right", false);

            anime.SetBool("Front", false);

            anime.SetBool("Back", false);

            anime.SetBool("RightWalk", false);

            anime.SetBool("LeftWalk", false);

            anime.SetBool("FrontWalk", false);

            anime.SetBool("BackWalk", false);
            //This removes all the other possible animation directions and sets it to face left
        }


        if (Input.GetKeyUp(KeyCode.S))
        {
    
            anime.SetBool("Front", true);

            anime.SetBool("Right", false);

            anime.SetBool("Left", false);

            anime.SetBool("Back", false);

            anime.SetBool("RightWalk", false);

            anime.SetBool("LeftWalk", false);

            anime.SetBool("FrontWalk", false);

            anime.SetBool("BackWalk", false);

            //This removes all the other possible animation directions and sets it to stand facing the viewer
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            anime.SetBool("RightWalk", false);

            anime.SetBool("LeftWalk", false);

            anime.SetBool("FrontWalk", false);

            anime.SetBool("BackWalk", false);

            anime.SetBool("Front", false);

            anime.SetBool("Right", true);

            anime.SetBool("Left", false);

            anime.SetBool("Back", false);
            //This removes all the other possible animation directions and sets it to stand right
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            anime.SetBool("RightWalk", false);

            anime.SetBool("LeftWalk", false);

            anime.SetBool("FrontWalk", false);

            anime.SetBool("BackWalk", true);

            anime.SetBool("Front", false);

            anime.SetBool("Right", false);

            anime.SetBool("Left", false);

            anime.SetBool("Back", false);

            //This removes all the other possible animation directions and sets it to walk away from the player
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            anime.SetBool("RightWalk", false);

            anime.SetBool("LeftWalk", true);

            anime.SetBool("FrontWalk", false);

            anime.SetBool("BackWalk", false);

            anime.SetBool("Front", false);

            anime.SetBool("Right", false);

            anime.SetBool("Left", false);

            anime.SetBool("Back", false);

            //This removes all the other possible animation directions and sets it to walk left
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            anime.SetBool("RightWalk", false);

            anime.SetBool("LeftWalk", false);

            anime.SetBool("FrontWalk", true);

            anime.SetBool("BackWalk", false);

            anime.SetBool("Front", false);

            anime.SetBool("Right", false);

            anime.SetBool("Left", false);

            anime.SetBool("Back", false);
            //This removes all the other possible animation directions and sets it to walk whilst facing the player
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            anime.SetBool("RightWalk", true);

                anime.SetBool("LeftWalk", false);

                anime.SetBool("FrontWalk", false);

                anime.SetBool("BackWalk", false);

                anime.SetBool("Front", false);

                anime.SetBool("Right", false);

                anime.SetBool("Left", false);

                anime.SetBool("Back", false);

            //This removes all the other possible animation directions and sets it to walk right
            }
        }
    }


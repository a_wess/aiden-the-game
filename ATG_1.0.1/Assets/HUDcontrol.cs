﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDcontrol : MonoBehaviour {
    //the HUD displays for the boss dungeon
    public Text TextH3;
    public Text TextMoney;
    public Text TextBossHealth;
    void Start () {
		
	}
	
	void Update () {
 
        TextH3.text = obtainplayervalues.CurrentAidy_HP.ToString() + "/200";
        TextMoney.text = "£" + obtainplayervalues.CurrentMoney.ToString();
        TextBossHealth.text = obtainbossvalues.bosshealth.ToString() + "/300";
        //this displays the money, boss hp and player hp
    }
}

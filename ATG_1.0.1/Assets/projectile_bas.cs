﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile_bas : MonoBehaviour
{
    //the projectile enemies movements
    public float enemy_health = 20;
    public float slimez_speed;
    public Transform player;
    public bool canmove;
    public GameObject spinningcoin;
    private List<GameObject> Coins = new List<GameObject>();
  
 
    void Start()
    {
        canmove = false;
        enemy_health = 20;
       
    }


    private void OnBecameVisible()
    {
       
        canmove = true;
      
        //the enemy can move when it comes into the camera's range
    }

    void Update()
    {
      

            if (canmove == true)
        {
            Vector2 toTarget = GameObject.Find("Player").transform.position - transform.position;
            transform.Translate(toTarget * slimez_speed * Time.deltaTime);
            //when the enemy is visible it is activated and can move towards the player
        }

      

    
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "AidyProjectile(Clone)")
        {
            obtainplayervalues.LevelEnemyCount = obtainplayervalues.LevelEnemyCount - 1;
            obtainplayervalues.ThisLevelScore = obtainplayervalues.ThisLevelScore + 1;
            GameObject newCoin = (GameObject)Instantiate(spinningcoin, transform.position, Quaternion.identity);
            Coins.Add(newCoin);
            obtainplayervalues.kills = obtainplayervalues.kills + 1;
            Debug.Log(obtainplayervalues.kills + "kills");

            gameObject.SetActive(false);
            //the score increases, a coin is dropped and the enemy is destroyed when the player hits it with a shuriken

        }
    }

}
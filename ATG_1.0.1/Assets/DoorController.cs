﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class DoorController : MonoBehaviour {
    public Transform fifthdoor;
    public GameObject doorfifth;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(obtainplayervalues.BossUnlocked())
            fifthdoor.gameObject.SetActive(true);  
	}
    void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.name == "First door")
        {
            SceneManager.LoadScene("slimez");
            obtainplayervalues.LevelEnemyCount = 12;

        }
        //this loads the slimez dungeon if the player hits the first door

        if (collision.gameObject.name == "second door")
        {
            SceneManager.LoadScene("robots");
            obtainplayervalues.LevelEnemyCount = 11;
        }
        //this loads the robots dungeon if the player hits the second door

        if (collision.gameObject.name == "third door")
        {
            SceneManager.LoadScene("anarchy");
            obtainplayervalues.LevelEnemyCount = 17;

        }
        //this loads the anarchists dungeon if the player hits the third door
        if (collision.gameObject.name == "fourth")
        {
            SceneManager.LoadScene("Zombies");
            obtainplayervalues.LevelEnemyCount = 17;

        }
        //this loads the zombies dungeon if the player hits the fourth door
        if (collision.gameObject.name == "fifth")
        {
            SceneManager.LoadScene("final");

        }
        //this loads the final dungeon if the player hits the fifth door
        if (collision.gameObject.name == "fifth")
        {
            SceneManager.LoadScene("final");

        }
        //this loads the final dungeon if the player hits the fifth door
        if (collision.gameObject.name == "return")
        {
            if (obtainplayervalues.ThisLevelScore == 12)
            {
                SceneManager.LoadScene("walker");
                obtainplayervalues.levels[0] = true;
                Debug.Log("dungeon 1 complete");
            }
            //returns the player to the base if they killed all the enemies in the dungeon and sets the first level to won


        }
        if (collision.gameObject.name == "returnrobots")
        {
            if (obtainplayervalues.ThisLevelScore == 11)
            {
                SceneManager.LoadScene("walker");
                obtainplayervalues.levels[1] = true;
            }
            //returns the player to the base if they have killed all the enemies in the dungeon and sets the second level to won


        }
        if (collision.gameObject.name == "returnanarchy")
        {
            if (obtainplayervalues.ThisLevelScore == 17)
            {
                SceneManager.LoadScene("walker");
                obtainplayervalues.levels[2] = true;
            }
            //returns the player to the base if they have killed all the enemies in the dungeon and sets the third level to won


        }
        if (collision.gameObject.name == "returnzombies")
        {
            if (obtainplayervalues.ThisLevelScore == 17)
            {
                SceneManager.LoadScene("walker");
                obtainplayervalues.levels[3] = true;
                //returns the player to the base if they have killed all the enemies in the dungeon and sets the fourth level to won

            }
        }
        //this loads the cafe and sets the dungeon to beat if the player hits the return door
    }
}

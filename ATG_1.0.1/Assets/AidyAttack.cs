﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AidyAttack : MonoBehaviour
{
    //the player's ability to attack with projectiles
    private List<GameObject> Projectilesleft = new List<GameObject>();
    private List<GameObject> Projectilesright = new List<GameObject>();
    private List<GameObject> Projectilesup = new List<GameObject>();
    private List<GameObject> Projectilesdown = new List<GameObject>();
    public Rigidbody2D Projectect;
    public GameObject sword_projectile;
    public GameObject wall;
    public Transform Spawner;
    public float sanics;
    public float delay;
    public bool canshoot = true;
    public static GameObject fireSwordleft;
    public GameObject fireSwordright;
    public GameObject fireSworddown;
    public GameObject fireSwordup;
    public GameObject sword_left;
    public GameObject sword_right;
    public GameObject sword_backward;
    public GameObject sword_forward;
    public GameObject Player;
    public GameObject Sword;
    public static bool canthrow;
    public static bool canattack;

    public double wait = 5;
    public double swordcounter;
    private double projectilecounter;


    void Start()
    {
        Sword = new GameObject();
        canthrow = true;
        Physics2D.IgnoreCollision(Sword.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        sword_left = (GameObject)Instantiate(sword_left, transform.position, Quaternion.identity);
        sword_left.SetActive(false);
        sword_right = (GameObject)Instantiate(sword_right, transform.position, Quaternion.identity);
        sword_right.SetActive(false);
        sword_forward = (GameObject)Instantiate(sword_forward, transform.position, Quaternion.identity);
        sword_forward.SetActive(false);
        sword_backward = (GameObject)Instantiate(sword_backward, transform.position, Quaternion.identity);
        sword_backward.SetActive(false);
        projectilecounter = wait;
        swordcounter = wait;
        //this creates a delay between shots
    }


    void Update()
    {
        if (sword_left.activeInHierarchy)
            sword_left.transform.position = new Vector3(transform.position.x - 1, transform.position.y, transform.position.z);

        if (sword_right.activeInHierarchy)
            sword_right.transform.position = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);

        if (sword_backward.activeInHierarchy)
            sword_backward.transform.position = new Vector3(transform.position.x, transform.position.y - 2, transform.position.z);

        if (sword_forward.activeInHierarchy)
            sword_forward.transform.position = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);

        projectilecounter -= Time.deltaTime;
        swordcounter -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space) && swordcounter < 0)
        {
            swordcounter = wait;
            SwordAttack(obtainplayervalues.Facing);
            canattack = false;
        }
        

        if (canthrow)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow) && projectilecounter < 0)
            {
                projectilecounter = wait;
                //this refreshes the delay between shots
                GameObject throwing_swordleft = (GameObject)Instantiate(sword_projectile, transform.position, Quaternion.identity);


                Projectilesleft.Add(throwing_swordleft);
                canshoot = false;

                //this preduces one throwing sword for every keypress of Q
            }

            for (int i = 0; i < Projectilesleft.Count; i++)
            {
                fireSwordleft = Projectilesleft[i];
                if (fireSwordleft != null)
                {

                    fireSwordleft.transform.Translate(new Vector3(-1, 0) * Time.deltaTime * sanics);

                    //this moves the sword

                    Vector3 SwordScreenPos = Camera.main.WorldToScreenPoint(fireSwordleft.transform.position);
                    if (SwordScreenPos.y >= Screen.height || SwordScreenPos.x >= Screen.width)
                    {
                        DestroyObject(fireSwordleft);
                        Projectilesleft.Remove(fireSwordleft);

                        //this deletes the sword if it goes off the screen
                    }



                }
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (projectilecounter < 0)
                {
                    projectilecounter = wait;
                    //this refreshes the delay between shots
                    GameObject throwing_swordup = (GameObject)Instantiate(sword_projectile, transform.position, Quaternion.identity);
                    Physics2D.IgnoreCollision(Player.GetComponent<BoxCollider2D>(), throwing_swordup.GetComponent<BoxCollider2D>());

                    Projectilesup.Add(throwing_swordup);
                    canshoot = false;
                }
                //this preduces one throwing sword for every keypress of up
            }

            for (int i = 0; i < Projectilesup.Count; i++)
            {
                fireSwordup = Projectilesup[i];
                if (fireSwordup != null)
                {

                    fireSwordup.transform.Translate(new Vector3(0, 1) * Time.deltaTime * sanics);

                    //this moves the sword

                    Vector3 SwordScreenPos = Camera.main.WorldToScreenPoint(fireSwordup.transform.position);
                    if (SwordScreenPos.y >= Screen.height || SwordScreenPos.x >= Screen.width)
                    {
                        DestroyObject(fireSwordup);
                        Projectilesleft.Remove(fireSwordup);

                        //this deletes the sword if it goes off the screen
                    }



                }
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (projectilecounter < 0)
                {
                    projectilecounter = wait;
                    //this refreshes the delay between shots
                    GameObject throwing_swordright = (GameObject)Instantiate(sword_projectile, transform.position, Quaternion.identity);
                    Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(), throwing_swordright.GetComponent<BoxCollider2D>());

                    Projectilesright.Add(throwing_swordright);
                    canshoot = false;
                }
                //this produces a shuriken prefab whenever the player hits the right arrow key
            }

            for (int i = 0; i < Projectilesright.Count; i++)
            {
                fireSwordright = Projectilesright[i];
                if (fireSwordright != null)
                {

                    fireSwordright.transform.Translate(new Vector3(1, 0) * Time.deltaTime * sanics);

                    //this moves the shuriken to the right of the player

                    Vector3 SwordScreenPos = Camera.main.WorldToScreenPoint(fireSwordright.transform.position);
                    if (SwordScreenPos.y >= Screen.height || SwordScreenPos.x >= Screen.width)
                    {
                        DestroyObject(fireSwordright);
                        Projectilesright.Remove(fireSwordright);

                        //this deletes the shuriken if it goes off the screen
                    }



                }
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (projectilecounter < 0)
                {
                    projectilecounter = wait;
                    //this refreshes the delay between shots
                    GameObject throwing_sworddown = (GameObject)Instantiate(sword_projectile, transform.position, Quaternion.identity);
                    Physics2D.IgnoreCollision(Player.GetComponent<BoxCollider2D>(), throwing_sworddown.GetComponent<BoxCollider2D>());

                    Projectilesdown.Add(throwing_sworddown);
                    canshoot = false;
                }

            }

            for (int i = 0; i < Projectilesdown.Count; i++)
            {
                fireSworddown = Projectilesdown[i];
                if (fireSworddown != null)
                {

                    fireSworddown.transform.Translate(new Vector3(0, -1) * Time.deltaTime * sanics);

                    //this moves the sword

                    Vector3 SwordScreenPos = Camera.main.WorldToScreenPoint(fireSworddown.transform.position);
                    if (SwordScreenPos.y >= Screen.height || SwordScreenPos.x >= Screen.width)
                    {
                        DestroyObject(fireSworddown);
                        Projectilesdown.Remove(fireSworddown);

                        //this deletes the sword if it goes off the screen
                    }



                }
            }
            removeSword(obtainplayervalues.Facing);
        }
        if (obtainplayervalues.FastShuriken)
        {
            sanics = 15;
            //the shurikens increase in speed when the player buys the upgrade
        }
    }

    public void BuyFasterShuriken()
    {
        if (obtainplayervalues.CurrentMoney >= 100 && !obtainplayervalues.FastShuriken)
        {
            obtainplayervalues.CurrentMoney = obtainplayervalues.CurrentMoney - 100;
            obtainplayervalues.FastShuriken = true;
            sanics = 15;


            //this allows the player to upgrade to faster shuriken in the shop but the upgrade can only be bought once.
        }
    }

    public void SwordAttack(obtainplayervalues.Direction dir)
    {

        if (dir == obtainplayervalues.Direction.LEFT)
            sword_left.SetActive(true);
        else
            if (dir == obtainplayervalues.Direction.RIGHT)
            sword_right.SetActive(true);
        else
            if (dir == obtainplayervalues.Direction.FORWARD)
                sword_forward.SetActive(true);
            else
                    sword_backward.SetActive(true);
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
       

        if (collision.gameObject.name == "sword_forward(Clone)" || collision.gameObject.name == "sword_backward(Clone)" ||
            collision.gameObject.name == "sword_left(Clone)" || collision.gameObject.name == "sword_right(Clone)")
            Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(), collision.collider.GetComponent<BoxCollider2D>());

        if (collision.gameObject.name == "Player")
            Physics2D.IgnoreCollision(collision.collider.GetComponent<Collider2D>(), GetComponent<Collider2D>());


    }

    void removeSword(obtainplayervalues.Direction dir)
    {   if (Input.GetKeyUp(KeyCode.Space) || swordcounter < 0)
        {
            sword_left.SetActive(false);
            sword_right.SetActive(false);
            sword_forward.SetActive(false);
            sword_backward.SetActive(false);
        }
        if (dir != obtainplayervalues.Direction.LEFT)
            sword_left.SetActive(false);
        if (dir != obtainplayervalues.Direction.RIGHT)
            sword_right.SetActive(false);
        if (dir != obtainplayervalues.Direction.FORWARD)
            sword_forward.SetActive(false);
        if (dir != obtainplayervalues.Direction.BACKWARD)
            sword_backward.SetActive(false);

    }

}




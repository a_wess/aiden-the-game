﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eat_food : MonoBehaviour {
    //each of these routines allows the player to eat one item form the inventory 
    //the player cannot gain the HP and remove the item if there is not at least 1 count of the item in the inventory
    //if the HP would exceed 200, it is set to 200
    public void EatOneSalt()
    {
        if (obtainplayervalues.OwnedSalt >= 1)
        {
            
            obtainplayervalues.OwnedSalt = obtainplayervalues.OwnedSalt - 1;
            Debug.Log(obtainplayervalues.OwnedSalt + "salt");
      
            obtainplayervalues.CurrentAidy_HP = obtainplayervalues.CurrentAidy_HP + 2;
            if (obtainplayervalues.CurrentAidy_HP >= 200)
            {
                obtainplayervalues.CurrentAidy_HP = 200;
            }
        }

    }

    public void EatOneMeme()
    {
        if (obtainplayervalues.OwnedMemes >= 1)
        {
           
            obtainplayervalues.OwnedMemes = obtainplayervalues.OwnedMemes - 1;
            Debug.Log(obtainplayervalues.OwnedMemes.ToString() + "memes");
            obtainplayervalues.CurrentAidy_HP = obtainplayervalues.CurrentAidy_HP + 10;
            if (obtainplayervalues.CurrentAidy_HP >= 200)
            {
                obtainplayervalues.CurrentAidy_HP = 200;

            }
        }

    }
    public void EatOnePizzaCoke()
    {
        if (obtainplayervalues.OwnedPizzaCoke >= 1)
        {
            obtainplayervalues.OwnedPizzaCoke = obtainplayervalues.OwnedPizzaCoke - 1;
            obtainplayervalues.CurrentAidy_HP = obtainplayervalues.CurrentAidy_HP + 25;
            Debug.Log(obtainplayervalues.OwnedPizzaCoke.ToString() + "PC");
            if (obtainplayervalues.CurrentAidy_HP >= 200)
            {
                obtainplayervalues.CurrentAidy_HP = 200;        
            }
        }
    }
    public void EatOneCJ()
    {
        if (obtainplayervalues.OwnedCrackerJacks >= 1)
        {
            obtainplayervalues.OwnedCrackerJacks = obtainplayervalues.OwnedCrackerJacks - 1;
            obtainplayervalues.CurrentAidy_HP = obtainplayervalues.CurrentAidy_HP + 60;
            Debug.Log(obtainplayervalues.OwnedCrackerJacks.ToString() + "CJ");
            if (obtainplayervalues.CurrentAidy_HP >= 200)
            {
                obtainplayervalues.CurrentAidy_HP = 200;
            }
        }
    }
    public void EatOneRAWSteak()
    {
        if (obtainplayervalues.OwnedRAWSteak >= 1)
        {
            obtainplayervalues.OwnedRAWSteak = obtainplayervalues.OwnedRAWSteak - 1;
            obtainplayervalues.CurrentAidy_HP = obtainplayervalues.CurrentAidy_HP + 100;
            Debug.Log(obtainplayervalues.OwnedRAWSteak.ToString() + "RAWSteak");
            if (obtainplayervalues.CurrentAidy_HP >= 200)
            {
                obtainplayervalues.CurrentAidy_HP = 200;
            }
        }
    }
    public void EatOneMK()
    {
        if (obtainplayervalues.OwnedMK >= 1)
        {
            obtainplayervalues.OwnedMK = obtainplayervalues.OwnedMK - 1;
            obtainplayervalues.CurrentAidy_HP = obtainplayervalues.CurrentAidy_HP + 200;
            Debug.Log(obtainplayervalues.OwnedMK.ToString() + "MK");
            if (obtainplayervalues.CurrentAidy_HP >= 200)
            {
                obtainplayervalues.CurrentAidy_HP = 200;
            }
        }

    }
}

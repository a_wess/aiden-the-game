﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_move : MonoBehaviour {
    //the ingame camera movements in the dungeons
    Transform position1;
    public static bool canmove;
    void Start () {
        canmove = true;
       position1 = GameObject.Find("Player").transform; 
    }
    private void Update()
    {
        if (canmove)
        {
            transform.position = position1.position + new Vector3(0, 0, -10);
        }
        //this keeps the camera position stuck to the players character
    }
}

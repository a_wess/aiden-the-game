﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile_hit : MonoBehaviour
{
    //the controls for when a player lands a hit on an enemy
    public GameObject enemys;
    public GameObject projectile;
    public GameObject Player;
    public GameObject spinningcoin;
    public static bool collided;
    public static bool gottem;
    public int coinresult;
    public GameObject coin;
    public AudioClip hit;
    public GameObject anarchist;
    public GameObject zombies;
    public AudioClip KillEnemySound;
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        
        Physics2D.IgnoreCollision(projectile.GetComponent<BoxCollider2D>(), Player.GetComponent<BoxCollider2D>());
        Physics2D.IgnoreCollision(projectile.GetComponent<BoxCollider2D>(), coin.GetComponent<BoxCollider2D>());
        //the projectile does not collide with the player or coins
        

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        //each of these play the enemy kill sound and destroy the prokectile
        if (collision.gameObject.name == "slime")
        {
            
            AudioSource.PlayClipAtPoint(KillEnemySound, transform.position);
            DestroyObject(gameObject);
      
        }
        if (collision.gameObject.name == "robots")
        {
           
            AudioSource.PlayClipAtPoint(KillEnemySound, transform.position);
            DestroyObject(gameObject);    
            
               
        }
        if (collision.gameObject.name == "anarchist")
        {
          
            AudioSource.PlayClipAtPoint(KillEnemySound, transform.position);
            DestroyObject(gameObject);
      //      anarchist.SetActive(false);
         
        }
        if (collision.gameObject.name == "zombie")
        {
            
            AudioSource.PlayClipAtPoint(KillEnemySound, transform.position);
            DestroyObject(gameObject);
         //   zombies.SetActive(false);
         
            
        }

        if (collision.gameObject.name == "WALL")
        {
            DestroyObject(gameObject);
            //this destroys the projectile when it hits a wall
        }

        if (collision.gameObject.name == "boss")
        {
            obtainbossvalues.bosshealth = obtainbossvalues.bosshealth - 10;
            DestroyObject(gameObject);

            //the boss takes loses 10 health points when the player successfully hits it with a projectile
        }
    }
    private void OnBecameInvisible()
    {
       
        DestroyObject(gameObject);
       //the projectile is destroyed when it leaves the camera's vision
    }
    
}    
    




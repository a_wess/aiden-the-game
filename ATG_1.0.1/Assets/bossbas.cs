﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossbas : MonoBehaviour {
    //the boss movement
    public float boss_speed;
    public Transform player;
    public bool canmove;

 
    void Start()
    {

    }

  

    private void OnBecameVisible()
    {
        canmove = true;
        //the boss can move if they become visible
    }


    void Update()
    {


        if (canmove)
        {
            Vector2 toTarget = GameObject.Find("Player").transform.position - transform.position;
            transform.Translate(toTarget * boss_speed * Time.deltaTime);
            //the boss moves towards the player
        }

        

    }

    void OnCollisionEnter2D(Collision2D collision)
    {


        if (collision.gameObject.name == "Player")
        {
            obtainplayervalues.CurrentAidy_HP = obtainplayervalues.CurrentAidy_HP - 10;

           //the player takes 10 damage points when the boss hits them

        }

     
        

    }
}

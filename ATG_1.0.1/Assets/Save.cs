﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Save : MonoBehaviour {
    //the save and load functions

	void Start () {
		
	}
	
	
	void Update () {
		
	}
    public void SavetheGame()
    {
        PlayerPrefs.SetFloat("PlayerMoney", obtainplayervalues.CurrentMoney);
        PlayerPrefs.SetInt("Memes", obtainplayervalues.OwnedMemes);
        PlayerPrefs.SetInt("Salts", obtainplayervalues.OwnedSalt);
        PlayerPrefs.SetInt("CJ", obtainplayervalues.OwnedCrackerJacks);
        PlayerPrefs.SetInt("Memes", obtainplayervalues.OwnedMemes);
        PlayerPrefs.SetInt("PC", obtainplayervalues.OwnedPizzaCoke);
        PlayerPrefs.SetInt("Steak", obtainplayervalues.OwnedRAWSteak);
        PlayerPrefs.SetInt("MK", obtainplayervalues.OwnedMK);
        PlayerPrefs.SetInt("kills", obtainplayervalues.kills);
        PlayerPrefs.SetInt("FastShuriken", (obtainplayervalues.FastShuriken ? 1 : 0));
        PlayerPrefs.SetInt("SharpShuriken", (obtainplayervalues.SharpShuriken ? 1 : 0));
        PlayerPrefs.SetInt("SlimezWin", (obtainplayervalues.levels[0] ? 1 : 0));
        PlayerPrefs.SetInt("RobotsWin", (obtainplayervalues.levels[1] ? 1 : 0));
        PlayerPrefs.SetInt("ZombieWin", (obtainplayervalues.levels[3] ? 1 : 0));
        PlayerPrefs.SetInt("AnarchyWin", (obtainplayervalues.levels[2] ? 1 : 0));
        //The players money, inventory and progress are logged into the first save registry

    }
    public void SavetheGame2()
    {
        PlayerPrefs.SetFloat("PlayerMoney2", obtainplayervalues.CurrentMoney);
        PlayerPrefs.SetInt("Memes2", obtainplayervalues.OwnedMemes);
        PlayerPrefs.SetInt("Salts2", obtainplayervalues.OwnedSalt);
        PlayerPrefs.SetInt("CJ2", obtainplayervalues.OwnedCrackerJacks);
        PlayerPrefs.SetInt("Memes2", obtainplayervalues.OwnedMemes);
        PlayerPrefs.SetInt("PC2", obtainplayervalues.OwnedPizzaCoke);
        PlayerPrefs.SetInt("Steak2", obtainplayervalues.OwnedRAWSteak);
        PlayerPrefs.SetInt("MK2", obtainplayervalues.OwnedMK);
        PlayerPrefs.SetInt("kills2", obtainplayervalues.kills);
        PlayerPrefs.SetInt("FastShuriken2", (obtainplayervalues.FastShuriken ? 1 : 0));
        PlayerPrefs.SetInt("SharpShuriken2", (obtainplayervalues.SharpShuriken ? 1 : 0));
        PlayerPrefs.SetInt("SlimezWin2", (obtainplayervalues.levels[0] ? 1 : 0));
        PlayerPrefs.SetInt("RobotsWin2", (obtainplayervalues.levels[1] ? 1 : 0));
        PlayerPrefs.SetInt("ZombieWin2", (obtainplayervalues.levels[3] ? 1 : 0));
        PlayerPrefs.SetInt("AnarchyWin2", (obtainplayervalues.levels[2] ? 1 : 0));
        //The players money, inventory and progress are logged into the second save registry

    }

    public void SavetheGame3()
    {
        PlayerPrefs.SetFloat("PlayerMoney3", obtainplayervalues.CurrentMoney);
        PlayerPrefs.SetInt("Memes3", obtainplayervalues.OwnedMemes);
        PlayerPrefs.SetInt("Salts3", obtainplayervalues.OwnedSalt);
        PlayerPrefs.SetInt("CJ3", obtainplayervalues.OwnedCrackerJacks);
        PlayerPrefs.SetInt("Memes3", obtainplayervalues.OwnedMemes);
        PlayerPrefs.SetInt("PC3", obtainplayervalues.OwnedPizzaCoke);
        PlayerPrefs.SetInt("Steak3", obtainplayervalues.OwnedRAWSteak);
        PlayerPrefs.SetInt("MK3", obtainplayervalues.OwnedMK);
        PlayerPrefs.SetInt("kills3", obtainplayervalues.kills);
        PlayerPrefs.SetInt("FastShuriken3", (obtainplayervalues.FastShuriken ? 1 : 0));
        PlayerPrefs.SetInt("SharpShuriken3", (obtainplayervalues.SharpShuriken ? 1 : 0));
        PlayerPrefs.SetInt("SlimezWin3", (obtainplayervalues.levels[0] ? 1 : 0));
        PlayerPrefs.SetInt("RobotsWin3", (obtainplayervalues.levels[1] ? 1 : 0));
        PlayerPrefs.SetInt("ZombieWin3", (obtainplayervalues.levels[3] ? 1 : 0));
        PlayerPrefs.SetInt("AnarchyWin3", (obtainplayervalues.levels[2] ? 1 : 0));
        //The players money, inventory and progress are logged into the third save registry

    }
    public void LoadtheGame()
    {
        obtainplayervalues.CurrentMoney = PlayerPrefs.GetFloat("PlayerMoney");
        obtainplayervalues.OwnedMemes = PlayerPrefs.GetInt("Memes");
        obtainplayervalues.OwnedSalt = PlayerPrefs.GetInt("Salts");
        obtainplayervalues.OwnedCrackerJacks = PlayerPrefs.GetInt("CJ");
        obtainplayervalues.OwnedMemes = PlayerPrefs.GetInt("Memes");
        obtainplayervalues.OwnedPizzaCoke = PlayerPrefs.GetInt("PC");
        obtainplayervalues.OwnedRAWSteak = PlayerPrefs.GetInt("Steak");
        obtainplayervalues.OwnedMK = PlayerPrefs.GetInt("MK");
        obtainplayervalues.kills = PlayerPrefs.GetInt("kills");
        obtainplayervalues.FastShuriken = (PlayerPrefs.GetInt("FastShuriken") != 0);
        obtainplayervalues.SharpShuriken = (PlayerPrefs.GetInt("SharpShuriken") != 0);
        obtainplayervalues.levels[0] = (PlayerPrefs.GetInt("SlimezWin") != 0);
        obtainplayervalues.levels[1] = (PlayerPrefs.GetInt("RobotsWin") != 0);
        obtainplayervalues.levels[2] = (PlayerPrefs.GetInt("AnarchyWin") != 0);
        obtainplayervalues.levels[3] = (PlayerPrefs.GetInt("ZombieWin") != 0);

        //The players money, inventory and progress are taken from the first save registry
    }

    public void LoadtheGame2()
    {
        obtainplayervalues.CurrentMoney = PlayerPrefs.GetFloat("PlayerMoney2");
        obtainplayervalues.OwnedMemes = PlayerPrefs.GetInt("Memes2");
        obtainplayervalues.OwnedSalt = PlayerPrefs.GetInt("Salts2");
        obtainplayervalues.OwnedCrackerJacks = PlayerPrefs.GetInt("CJ2");
        obtainplayervalues.OwnedMemes = PlayerPrefs.GetInt("Memes2");
        obtainplayervalues.OwnedPizzaCoke = PlayerPrefs.GetInt("PC2");
        obtainplayervalues.OwnedRAWSteak = PlayerPrefs.GetInt("Steak2");
        obtainplayervalues.OwnedMK = PlayerPrefs.GetInt("MK2");
        obtainplayervalues.kills = PlayerPrefs.GetInt("kills2");
        obtainplayervalues.FastShuriken = (PlayerPrefs.GetInt("FastShuriken2") != 0);
        obtainplayervalues.SharpShuriken = (PlayerPrefs.GetInt("SharpShuriken2") != 0);
        obtainplayervalues.levels[0] = (PlayerPrefs.GetInt("SlimezWin2") != 0);
        obtainplayervalues.levels[1] = (PlayerPrefs.GetInt("RobotsWin2") != 0);
        obtainplayervalues.levels[2] = (PlayerPrefs.GetInt("AnarchyWin2") != 0);
        obtainplayervalues.levels[3] = (PlayerPrefs.GetInt("ZombieWin2") != 0);

        //The players money, inventory and progress are taken from the second save registry
    }

    public void LoadtheGame3()
    {
        obtainplayervalues.CurrentMoney = PlayerPrefs.GetFloat("PlayerMoney3");
        obtainplayervalues.OwnedMemes = PlayerPrefs.GetInt("Memes3");
        obtainplayervalues.OwnedSalt = PlayerPrefs.GetInt("Salts3");
        obtainplayervalues.OwnedCrackerJacks = PlayerPrefs.GetInt("CJ3");
        obtainplayervalues.OwnedMemes = PlayerPrefs.GetInt("Memes3");
        obtainplayervalues.OwnedPizzaCoke = PlayerPrefs.GetInt("PC3");
        obtainplayervalues.OwnedRAWSteak = PlayerPrefs.GetInt("Steak3");
        obtainplayervalues.OwnedMK = PlayerPrefs.GetInt("MK3");
        obtainplayervalues.kills = PlayerPrefs.GetInt("kills3");
        obtainplayervalues.FastShuriken = (PlayerPrefs.GetInt("FastShuriken3") != 0);
        obtainplayervalues.SharpShuriken = (PlayerPrefs.GetInt("SharpShuriken3") != 0);
        obtainplayervalues.levels[0] = (PlayerPrefs.GetInt("SlimezWin3") != 0);
        obtainplayervalues.levels[1] = (PlayerPrefs.GetInt("RobotsWin3") != 0);
        obtainplayervalues.levels[2] = (PlayerPrefs.GetInt("AnarchyWin3") != 0);
        obtainplayervalues.levels[3] = (PlayerPrefs.GetInt("ZombieWin3") != 0);

        //The players money, inventory and progress are taken from the third save registry
    }

}

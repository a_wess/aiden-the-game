﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_ignore_collide : MonoBehaviour {
    public GameObject projectile;
    public GameObject enemy;
    public GameObject coin;
	
	void Start () {
		
	}
	
	
	void Update () {
        Physics2D.IgnoreCollision(projectile.GetComponent<BoxCollider2D>(), enemy.GetComponent<BoxCollider2D>());
        Physics2D.IgnoreCollision(projectile.GetComponent<BoxCollider2D>(), coin.GetComponent<BoxCollider2D>());
        Physics2D.IgnoreCollision(projectile.GetComponent<BoxCollider2D>(), projectile.GetComponent<BoxCollider2D>());
        //the enemys projectiles/mines do not collide with coins, projectiles or the enemy themselves
        
    }
}
